#include <GL\glew.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <cstdio>
#include <cassert>

#include "GL_framework.h"

#include <vector>

namespace ImGui {
	extern void Render();
}
namespace Box {
	extern void setupCube();
	extern void cleanupCube();
	extern void drawCube();
}
namespace Axis {
	extern void setupAxis();
	extern void cleanupAxis();
	extern void drawAxis();
}

namespace Cube {
	extern void setupCube();
	extern void cleanupCube();
	extern void updateCube(const glm::mat4& transform);
	extern void drawCube();
	void drawParallelepiped(double currentTime, float translate[3], float scale[3], float rgb[3]);
	extern GLuint cubeVao;
	extern GLuint cubeProgram;
	extern glm::mat4 objMat;
	extern int numVerts;
}
namespace RenderVars {
	extern const float FOV;
	extern const float zNear;
	extern const float zFar;

	extern glm::mat4 _projection;
	extern glm::mat4 _modelView;
	extern glm::mat4 _MVP;
	extern glm::mat4 _inv_modelview;
	extern glm::vec4 _cameraPoint;

	extern struct prevMouse {
		float lastx, lasty;
		MouseEvent::Button button = MouseEvent::Button::None;
		bool waspressed = false;
	};

	extern float panv[3];
	extern float rota[2];
}
namespace RV = RenderVars;

const float i_pTranslates[7][3] = { { 1.f, 1.5f, -2.f },{ -2.f, 4.5f, -3.f },{ 3.0f, 7.0f, -3.f },{ -5.0f, 7.0f, -3.f },{ 5.0f, 7.0f, -3.f },{ -4.0f, 2.0f, 2.0f },{ 0.0f, 5.0f, 0.0f } };
float pTranslates[7][3] = { {1.f, 1.5f, -2.f },{ -2.f, 4.5f, -3.f },{ 3.0f, 7.0f, -3.f },{ -5.0f, 7.0f, -3.f },{ 5.0f, 7.0f, -3.f },{ -4.0f, 2.0f, 2.0f },{ 0.0f, 5.0f, 0.0f } };
float pScales[7][3] = { { 4.f, 3.f, 4.f },{ 3.f, 3.f, 3.f },{ 1.f, 2.f, 2.f } ,{ 1.f, 2.f, 2.f } ,{ 1.f, 2.f, 2.f } ,{ 1.f, 4.f, 2.f },{ 1.f, 6.f, 1.f } };
float pRGB[7][3] = { { 0.3f, 1.0f, 0.6f },{ 0.4f, 0.5f, 0.7f },{ 1.f, 0.6f, 0.8f },{ 0.4f, 0.4f, 1.f },{ 1.f, 0.1f, 1.f } ,{ 1.f, 1.f, 0.7f },{ 1.f, 0.f, 0.f } };
bool pMove[7] = { true,true,true,true,true,false,false };

int pSize = 7;
bool first = true;


enum cameraEffects { LTRAVELLING, RTRAVELLING, ZOOMIN, ZOOMOUT, INCREASEFOV, DECREASEFOV, DOLLY, INVERSEDOLLY };
cameraEffects currentEffect = cameraEffects::LTRAVELLING;

void resetVars(){
	//pTranslates[7][3] = { { 0.f, 1.5f, -3.f },{ 0.f, 4.5f, -3.f },{ 1.0f, 7.0f, -3.f },{ -5.0f, 7.0f, -3.f },{ 5.0f, 7.0f, -3.f },{ -1.0f, 2.0f, 3.0f },{ 0.0f, 0.0f, 0.0f } };
	//pScales[7][3] = { { 8.f, 3.f, 4.f },{ 8.f, 3.f, 3.f },{ 1.f, 2.f, 2.f } ,{ 1.f, 2.f, 2.f } ,{ 1.f, 2.f, 2.f } ,{ 1.f, 4.f, 2.f },{ 1.f, 6.f, 1.f } };
	//pRGB[7][3] = { { 0.6f, 1.0f, 0.6f },{ 1.f, 0.6f, 1.f },{ 1.f, 0.6f, 1.f },{ 1.f, 0.6f, 1.f },{ 1.f, 0.6f, 1.f } ,{ 1.f, 1.f, 0.7f },{ 1.f, 1.f, 0.7f } };
	//pMove[7] = { true,true,true,true,true,false,false };
}

void myInitCode() {
	RV::panv[0] = -4.f;

	// Setup shaders & geometry
	Box::setupCube();
	Cube::setupCube();
}

void update(double currentTime) {
	float vLateral = 0.f;
	float vZoom = 0.f;

	switch (currentEffect)
	{
	case cameraEffects::LTRAVELLING: {
		RV::panv[0] += 0.05f;
		if (RV::panv[0] >= 0.f)
		{
			currentEffect = cameraEffects::ZOOMIN;
		}
		break;
	}
	case cameraEffects::RTRAVELLING: {
		RV::panv[0] -= 0.05f;
		if (RV::panv[0] <= -4.f)
		{
			currentEffect = cameraEffects::LTRAVELLING;
		}
		break;
	}
	case cameraEffects::ZOOMIN: {
		RV::panv[2] += 0.05f;
		if (RV::panv[2] > -5)
		{
			currentEffect = cameraEffects::INCREASEFOV;
		}
		break;
	}
	case cameraEffects::ZOOMOUT: {
		RV::panv[2] -= 0.05f;
		break;
	}
	case cameraEffects::INCREASEFOV: {
		for (size_t i = 0; i < pSize; i++)
		{
			if (pMove[i])
			{
				pTranslates[i][2] -= 0.05f;
				pScales[i][0] -= 0.01f;
				pScales[i][1] -= 0.01f;
				if (pTranslates[i][2] < i_pTranslates[i][2] -3.f)
				{
					currentEffect = cameraEffects::DOLLY;
				}
			}
			
		}
		break;
	}
	case cameraEffects::DECREASEFOV: {
		for (size_t i = 0; i < pSize; i++)
		{
			if (pMove[i])
			{
				pTranslates[i][2] += 0.05f;
				pScales[i][0] += 0.01f;
				pScales[i][1] += 0.01f;
				if (pTranslates[i][2] > i_pTranslates[i][2])
				{
					currentEffect = cameraEffects::DOLLY;
				}
			}
		}
		break;
	}
	case cameraEffects::DOLLY: {
		RV::panv[2] -= 0.05f;
		for (size_t i = 0; i < pSize; i++)
		{
			if (pMove[i])
			{
				pTranslates[i][2] += 0.05f;
				pScales[i][0] += 0.01f;
				pScales[i][1] += 0.01f;
				if (pTranslates[i][2] > i_pTranslates[i][2])
				{
					if (first)
					{
						currentEffect = cameraEffects::INVERSEDOLLY;
						first = false;
					}
					else {
						currentEffect = cameraEffects::RTRAVELLING;
						first = true;
					}
					
				}
			}
		}
		break;
	}
	case cameraEffects::INVERSEDOLLY: {
		RV::panv[2] += 0.05f;
		for (size_t i = 0; i < pSize; i++)
		{
			if (pMove[i])
			{
				pTranslates[i][2] -= 0.05f;
				pScales[i][0] -= 0.01f;
				pScales[i][1] -= 0.01f;
				if (pTranslates[i][2] < i_pTranslates[i][2] - 3.f)
				{
					currentEffect = cameraEffects::DOLLY;
				}
			}

		}
		break;
	}
	default:
		break;
	}
}

void draw(double currentTime) {
	//dolly in zoom out - alejar fondo mantener centro
	for (size_t i = 0; i < pSize; i++)
	{
		Cube::drawParallelepiped(currentTime, pTranslates[i], pScales[i], pRGB[i]);
	}
}

void myRenderCode(double currentTime) {
	//update positions
	update(currentTime);

	// render code
	Box::drawCube();
	draw(currentTime);
	ImGui::Render();

}
namespace Cube {
	void drawParallelepiped(double currentTime, float translate[3], float scale[3], float rgb[3]) {
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glm::mat4 t = glm::translate(glm::mat4(), glm::vec3(translate[0], translate[1], translate[2]));
		glm::mat4 s = glm::scale(glm::mat4(), glm::vec3(scale[0], scale[1], scale[2]));
		objMat = t * s;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), rgb[0], rgb[1], rgb[2], 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);
	}
}

void myCleanupCode() {
	Box::cleanupCube();
	Cube::cleanupCube();
}